from flask import Flask, json, session, request
import pickle
api = Flask(__name__)
@api.route('/savedata', methods=['POST'])
def save_data():
    data = request.get_json()
    pfile = open('pfile', 'wb')

    # source, destination
    pickle.dump(data, pfile)
    pfile.close()
    return 'Data Stored/Overwritten'



@api.route('/getdata', methods=['GET'])
def get_data():
    pfile = open('pfile', 'rb')
    data = pickle.load(pfile)
    pfile.close()
    return json.dumps(data)

if __name__ == '__main__':
    api.secret_key = 'kljasdf%$%$%kjlashdf234ey'
    #api.config['SESSION_TYPE'] = 'filesystem'

    api.run(host='0.0.0.0', port='8080', debug=True)